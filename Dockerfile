############################
# STEP 0 build arguments
############################
ARG ALPINE_IMAGE=alpine:latest
ARG GO_BUILDER_IMAGE=golang:1.19-alpine

############################
# STEP 1 build executable binary
############################
FROM ${GO_BUILDER_IMAGE} AS builder

LABEL maintainer="matterpale"

RUN apk update && apk add --no-cache git

WORKDIR /app

# Setup Go environment.
ENV GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0 \
    GOAMD64=v3

# Download dependencies.
COPY go.mod go.sum ./

COPY . .

RUN go build ./cmd/caller

############################
# STEP 2 build a small image
############################
FROM ${ALPINE_IMAGE}

# Install SSL ca certificates.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache ca-certificates tzdata && update-ca-certificates

RUN apk --no-cache add curl

# Copy our static executable.
COPY --from=builder /app/caller /bin/caller

# Use an unprivileged user.
USER daemon

EXPOSE 8130

CMD ["/bin/caller"]