A helper repo whose only purpose is to make GRPC calls on the [hostnamer](https://gitlab.com/matterpale/hostnamer) service and thus check the **client-side
round-robin** load-balancing policy.
