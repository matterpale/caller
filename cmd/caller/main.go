package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/matterpale/geronimo/internal/config"
	hostnamer "gitlab.com/matterpale/hostnamer/pkg/grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/resolver"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Panic("loading config")
	}

	resolver.SetDefaultScheme("dns")
	target := fmt.Sprintf("%s:%d", cfg.Address, cfg.Port)
	conn, err := grpc.Dial(target,
		// grpc.WithDefaultServiceConfig(`{"loadBalancingPolicy": "round_robin"}`),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Panicf("dialing GRPC address: %s, error: %v", target, err)
	}

	client := hostnamer.NewHostnamerClient(conn)
	ctx := context.Background()
	ticker := time.NewTicker(5 * time.Second)
	done := make(chan bool)

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				resp, err := client.Get(ctx, &hostnamer.Request{})
				if err != nil {
					log.Panic("calling hostnamer")
				}
				log.Println(resp.Hostname)
			}
		}
	}()

	time.Sleep(time.Minute)
	ticker.Stop()
	done <- true
}
