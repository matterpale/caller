package config

import (
	"os"
	"time"
)

const (
	address = "GRPC_ADDRESS"
	port    = "GRPC_PORT"
)

// Config contains all configurations.
type Config struct {
	Address                string
	Port                   int
	ServerKeepaliveTimeout time.Duration
	ShutdownGracePeriod    time.Duration
}

func Load() (*Config, error) {
	return &Config{
		Address:                os.Getenv(address),
		Port:                   7000,
		ServerKeepaliveTimeout: 20 * time.Second,
		ShutdownGracePeriod:    30 * time.Second,
	}, nil
}
